package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Cliente;

public class ClienteDAO {

    private DataSource dataSource;

    public ClienteDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public ArrayList<Cliente> readAll() {
        try {
            String SQL = "SELECT * from clientes";
            PreparedStatement ps = dataSource.getConnection().prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();

            ArrayList<Cliente> lista = new ArrayList<Cliente>();
            while (rs.next()) {
                Cliente cli = new Cliente();
                cli.setId(rs.getInt("id"));
                cli.setNome(rs.getString("nome"));
                cli.setEmail(rs.getString("email"));
                cli.setTelefone(rs.getString("telefone"));
                lista.add(cli);
            }
            ps.close();
            return lista;
        } catch (SQLException ex) {
            System.out.println("Erro ao recuperar" + ex);
        }
        return null;
    }

    public void inserir(String nome, String email, String telefone) {
        try {
            String comando = "insert into clientes (nome, email,telefone)values('" + nome + "','" + email + "','" + telefone + "')";
            PreparedStatement ps = dataSource.getConnection().prepareStatement(comando);

            ps.executeUpdate(comando);
            ps.close();
            JOptionPane.showMessageDialog(null,"Cliente inserido com sucesso.");
        } catch (SQLException er) {
            System.out.println("Erro ao inserir!!" + er);
        }
    }

    public void remover(int id) {
        try {
            String comando = "Delete from clientes where id = " + id;
            PreparedStatement ps = dataSource.getConnection().prepareStatement(comando);
            ps.executeUpdate(comando);
            JOptionPane.showMessageDialog(null, "Cliente removido com sucesso.");
        } catch (SQLException er) {
            System.out.println("Erro ao remover!\n" + er);
        }
    }
}
